<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>{{ title | title }}</title>

    <link rel="icon" type="image/ico" href="images/favicon.png" sizes="32x32">

    <!-- build:css styles/vendor.css -->
    <link rel="stylesheet" href="styles/normalize.css">
    <link rel="stylesheet" href="styles/bootstrap-grid.min.css">
    <!-- endbuild -->
    <!-- build:css styles/fonts.css -->
    <link href="styles/fonts.css" rel="stylesheet">
    <!-- endbuild -->
    <!-- build:css styles/main.css -->
    <link href="styles/main.css" rel="stylesheet">
    <!-- endbuild -->


</head>
<body>

<div class="page-wrapper">
    {% include('header2.html') %}
    <div class="page-content">
        {% block content %} {% endblock %}
    </div>
    {% include('footer2.html') %}
</div>

{% block scripts %} {% endblock %}


<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenMax.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/utils/Draggable.min.js"></script>




<script>

    $("#wrap").mousemove(function(e) {
        parallaxIt(e, ".hamburger", -100);
        parallaxIt(e, ".banner-content", -30);
    });

    function parallaxIt(e, target, movement) {
        var $this = $("#wrap");
        var relX = e.pageX - $this.offset().left;
        var relY = e.pageY - $this.offset().top;

        TweenMax.to(target, 1, {
            x: (relX - $this.width() / 2) / $this.width() * movement,
            y: (relY - $this.height() / 2) / $this.height() * movement
        });
    }
</script>

</body>

</html>
